<?php
namespace LittleSystems\Bitbucket_Webhook;
/* 
 * Copyright (C) 2016 Little Systems
 *
 * You may distribute under the terms of the Artistic License 2.0, as specified in the README.license file.
 */

function my_autoloader($myClass) 
{
    $myParts  = explode('\\', $myClass);
    $myClass  = end($myParts);
    $myFile   = __DIR__."/classes/$myClass.php";
  
    if (file_exists($myFile)) {
        include $myFile;
    }
}

function my_download($myUrl, $myUsername, $myPassword, $myPath) 
{
    try {
        $fp = fopen($myPath, "w");
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $myUrl);
        curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($ch, CURLOPT_USERPWD, $myUsername . ":" . $myPassword);
        curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        $resp = curl_exec($ch);

        // validate CURL status
        if (curl_errno($ch)) {
            throw new \Exception(curl_error($ch), 500);
        }

        // validate HTTP status code (user/password credential issues)
        $status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        if ($status_code != 200) {
            throw new \Exception("Response with Status Code [" . $status_code . "].", 500);
        }
    } 
    catch(Exception $ex) {
        if ($ch != null) curl_close($ch);
        if ($fp != null) fclose($fp);
        throw new \Exception('Unable to properly download file from url=[' + $url + '] to path [' + $destination + '].', 500, $ex);
    }
    
    if ($ch != null) curl_close($ch);
    if ($fp != null) fclose($fp);
}