<?php
namespace LittleSystems\Bitbucket_Webhook;
/*
 * Copyright (C) 2016 Little Systems
 *
 * You may distribute under the terms of the Artistic License 2.0, as specified in the README.license file.
 */

/**
 * Handler
 *
 * @author Little Systems
 */
class Handler 
{
    const BITBUCKET_URL     = 'https://bitbucket.org';
    const BITBUCKET_PACKAGE = 'master.tar.gz';
    
    public function __construct($myPath) 
    {
        $this->config = $this->loadIni("$myPath/config.ini");
        $this->trust  = $this->loadIni("$myPath/trust.ini");
    }

    public function readHook() 
    {
        $this->hook = new Hook;
    }
  
    public function handleHook() 
    {
        $this->repository = new Repository($this->hook);
        
        if (!$this->hook->isMasterBranchPush()) {
            return;
        }
        
        /* 
         * Determine instances to process
         * If instance CSV is provided as a GET variable, use it.
         * Otherwise, load all trusted instances (if any) for the user and/or group, of the application.
         */
        if (isset($this->hook->get['instance'])) {
            $myInstances = str_getcsv($this->hook->get['instance']);
        } elseif (is_array($this->trust[$this->repository->ownerUsername][$this->hook->get['application']])) {
            $myInstances = array_keys($this->trust[$this->repository->ownerUsername][$this->hook->get['application']]);
        } else {
            $myInstances = [];
        }
    
        if (!count($myInstances)) {
            // process application (standalone?) without instances
            if (isset($this->trust[$this->repository->ownerUsername][$this->hook->get['application']])) {
                // trusted
                $this->handleRepository();
            }
        } else {
            // process instances
            foreach ($myInstances as $myInstance) {
                if (isset($this->trust[$this->repository->ownerUsername][$this->hook->get['application']][$myInstance])) {
                    // trusted
                    $this->handleRepository($myInstance);
                }
            }
        }
    }
    
    private function handleRepository($myInstance = null) {
        $this->downloadRepository($myInstance);
        $this->processRepository($myInstance);
    }
    
    private function downloadRepository($myInstance = null)
    {     
        $myUrl          = self::BITBUCKET_URL."/{$this->repository->fullName}/get/".self::BITBUCKET_PACKAGE;

        $myUsername     = $this->config['read'][$this->repository->ownerUsername];

        $myPassword     = $this->config['user'][$myUsername];

        $myPath         = $this->getRepositoryPath($myInstance);
        if (!file_exists($myPath)) {
            mkdir($myPath);
        }
        
        my_download($myUrl, $myUsername, $myPassword, $myPath.'/'.self::BITBUCKET_PACKAGE);
    }
    
    private function processRepository($myInstance = null)
    {
        $myPath = $this->getRepositoryPath($myInstance);
        
        exec("tar xvfz $myPath/".self::BITBUCKET_PACKAGE." -C $myPath --strip-components=1");
        
        unlink("$myPath/".self::BITBUCKET_PACKAGE);
    }
    
    private function getRepositoryPath($myInstance = null)
    {
        $myPath         = $this->config['path'][$this->hook->get['application']];
        if ($myInstance) {
            $myPath     = $myPath[$myInstance];
        }
        $myPath        .= $this->config['subpath'][$this->hook->get['application']][$this->hook->get['module']]."/{$this->repository->name}";
        
        return $myPath;
    }
    
    private function loadIni($myConfigPath) 
    {
        // parse ini file
        $myIni = file_exists($myConfigPath) ? parse_ini_file($myConfigPath, true) : 
            exit("$myConfigPath is required.<br \>Refer to sample .ini.");
    
        return $myIni;
    }
}