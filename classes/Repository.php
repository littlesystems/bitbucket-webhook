<?php
namespace LittleSystems\Bitbucket_Webhook;
/*
 * Copyright (C) 2016 Little Systems
 *
 * You may distribute under the terms of the Artistic License 2.0, as specified in the README.license file.
 */

/**
 * Repository
 *
 * @author Little Systems
 */
class Repository 
{
    private $ownerUsername; // e.g. "littlesystems"
    private $fullName;      // e.g. "littlesystems/wp-builder-express"
    private $isPrivate;     // e.g. 1  
    private $name;          // e.g. "wp-builder-express"
  
    public function __construct($myHook) 
    {
        $post = $myHook->post;

        $this->ownerUsername  = $post["repository"]["owner"]["username"];
        $this->fullName       = $post["repository"]["full_name"];
        $this->isPrivate      = $post["repository"]["is_private"];
        $this->name           = substr($this->fullName, strlen($this->ownerUsername) + 1);
    }
  
    public function __get($myName) {
        return $this->$myName;
    }
}