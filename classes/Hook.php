<?php
namespace LittleSystems\Bitbucket_Webhook;
/*
 * Copyright (C) 2016 Little Systems
 *
 * You may distribute under the terms of the Artistic License 2.0, as specified in the README.license file.
 */

/**
 * Hook
 *
 * @author Little Systems
 */
class Hook 
{
    private $get;           // GET data
    private $post;          // POST data

    private $actorUsername; // e.g. "dstansfield"
    private $pushType;      // e.g. "branch"
    private $pushName;      // e.g. "master"
  
    public function __construct() 
    {
        $this->loadGet();
        $this->loadPost();
    }
  
    public function __get($myName) 
    {
        return $this->$myName;
    }

    public function isMasterBranchPush() 
    {
        return ($this->pushType == 'branch' && $this->pushName == 'master');
    }    
    
    /*
     * load GET data (optional)
     * - entered into the webhook URL, for example:
     *   https://hook.littlesystems.com.au/bitbucket/wordpress/?plugin=module
     */
    private function loadGet() 
    {
        $this->get = $_GET;
    }

    /*
     * load POST data
     * (credit to Stackoverflow user Ikke, http://stackoverflow.com/users/20261/ikke)
     */
    private function loadPost() 
    {
        $this->post = $post = json_decode(file_get_contents("php://input"), true) or die("No request.");

        // assign POST data
        $this->actorUsername  = $post["actor"]["username"];

        $this->pushType       = $post["push"]["changes"][0]["new"]["type"];
        $this->pushName       = $post["push"]["changes"][0]["new"]["name"];
    }
}