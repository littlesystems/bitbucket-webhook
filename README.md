# README.md #

Copyright (C) 2017 Little Systems (https://littlesystems.com.au).  May, 2017.

You may distribute under the terms of the Artistic License 2.0, as specified in the README.license file.

### What is this repository for? ###

**The repository is for distributing code from Bitbucket to Linux servers upon a master branch push.**  The handler is light weight and flexible, and requires Linux file editing and systems administration experience.

**The handler:**

* Reads the Bitbucket notification, ensures a master branch push request, and checks that the Bitucket code owner is trusted.
* Downloads code securely over HTTPS.
* Installs code for multiple application instances on the same Linux server, e.g. for all application instances, or for the live or development application instance only (as defined in URL GET arguments).
* Installs and unpacks code according to code types, e.g. plugin or theme (as defined in URL GET arguments). 

### How do I get set up? ###

**Requirements:** 

* An understanding of Linux systems administration and security.
* The PHP cURL library / extension.
* The [Apache 2 ITK MPM](http://mpm-itk.sesse.net/) or [PHP FPM](https://php-fpm.org/) for user-specific / privileged installation operations.
* The Bitbucket server IP addresses (see [here](https://confluence.atlassian.com/bitbucket/what-are-the-bitbucket-cloud-ip-addresses-i-should-use-to-configure-my-corporate-firewall-343343385.html)).

**General Installation Instructions:**

* Download the code and point the web server at the code.  To escalate filesystem permissions, use The Apache 2 MPM ITK or PHP FPM with due caution.  At least restrict the code to the known Bitbucket IP range.  (Also consider placing the .ini files in a parent directory).
* Configure the config.ini and trust.ini files.  Sample .ini files are included.
* Create Bitbucket webhooks for each server.  See the Atlassian documentation for instructions on creating webhooks.  Note that the URL of the webhook should be as configured above, and should include application, module, and instance (optional) GET parameters.  For example, https://hook.littlesystems.com.au/bitbucket-webhook/?application=wordpress&module=plugin&instance=littlesystems,development
* For testing GET parameters and POST data, consider using an application such as Postman for Google Chrome.  (Note also that Bitbucket now allows for requests (webhooks) to be resent, and for the details and the output to be viewed.)

### Who do I talk to? ###

* Please contact [Little Systems](https://littlesystems.com.au) for support.