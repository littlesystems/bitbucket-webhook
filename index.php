<?php
namespace LittleSystems\Bitbucket_Webhook;
/**
 * Little Systems Bitbucket Webhook Handler 2.1
 * 
 * Copyright (C) 2016 Little Systems (https://littlesystems.com.au).  December, 2016.
 *
 * You may distribute under the terms of the Artistic License 2.0, as specified in the README.license file.
 */

error_reporting(E_ALL);

require_once(__DIR__.'/functions.php');
spl_autoload_register(__NAMESPACE__.'\my_autoloader');

$handler = new Handler('./');

$handler->readHook();

$handler->handleHook();